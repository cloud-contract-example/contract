package contracts

import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description "should return even when number input is even"
    request{
        method GET()
        url("/validate/even") {
            queryParameters {
                parameter("num", "2")
            }
        }
    }
    response {
        body("Even")
        status 200
    }
}